\subsection{Tracts}
\label{sec:Tracts}

Tracts were introduced in~\cite{TRACTS11} as a specification and black-box testing mechanism for model transformations. They are a particular kind of \emph{model transformation contracts}~\cite{Baudry2006,Cariou04oclfor} especially well suited for specifying model transformations in a modular and tractable manner.
Tracts provide modular pieces of specification, each one focusing on a particular transformation scenario. Thus each model transformation can be specified by means of a set of Tracts, each one covering a specific use
case---which is defined in terms of particular input and output models and how
they should be related by the transformation. In this way, Tracts permit
partitioning the full input space of the transformation into smaller, more focused
behavioral units, and to define specific tests for them.
%Commonly, what developers are expected to do
%with Tracts is to identify the scenarios of interest~(each one defined by one Tract) and check
%whether the transformation behaves as expected in these scenarios.
Tracts also count on tool support for checking, in a black-box manner, that a given implementation behaves as expected---i.e., it respects the Tracts constraints~\cite{FaultLocMT}.

%\begin{figure}[t]
%\centering
%\includegraphics[width=\textwidth]{images/TMT}
%\caption{Building blocks of a tract as in~\cite{TRACTS11}.}
%\label{fig:TMT}
%\end{figure}

%Fig.~\ref{fig:TMT} depicts
The main components of the Tracts approach were described in~\cite{TRACTS11}: the source and target metamodels, the transformation $T$ under test, and the transformation contract, which consists of a Tract \emph{test suite} and a set of Tract constraints. In total, five different kinds of constraints are present: the general constraints defined for the source and target models, and the \emph{source}, \emph{target}, and \emph{source-target} Tract constraints imposed for a given transformation. %In the drawing, \code{mm} stands for metamodel, and \code{cd} is a short for class diagram.
%In a nutshell, a Tract defines a set of constraints on the \emph{source} and
%\emph{target} metamodels, a set of \emph{source-target} constraints, and a
%\emph{test suite}, i.e., a collection of source models.
%These constraints serve as ``contracts''~(in the sense of
%contract-based design~\cite{Meyer92}) for the transformation in some particular
%scenarios, and are expressed by means of OCL invariants. They provide the
%\emph{specification} of the transformation.

If we assume a source model $m$ being an element of the test suite and satisfying the source metamodel and the source Tract constraints given, the Tract
essentially requires the result $T(m)$ of applying transformation $T$ to satisfy the target metamodel and the target Tract constraints, and the tuple $\langle m,T(m)\rangle$ to satisfy the source-target Tract constraints. %In technical terms, a source tract constraint is basically an OCL expression with free variables over source elements, a target tract constraint has free variables over target elements, and a source-target tract constraint possesses free variables over source and target elements.

\begin{figure}[t]
\centering
\includegraphics[width=.8\linewidth]{images/bite2dobo_cd-invs}
\caption{Source and target metamodels.}
\label{fig:Tract}
\end{figure}


To illustrate Tracts, consider a simple model transformation, \code{BiBTex2DocBook}, that converts the information about proceedings of conferences~(in \code{BibTeX} format) into the corresponding information encoded in \code{DocBook} format\footnote{\url{http://docbook.org/}}. The source and target metamodels that we use for the transformation are shown in Fig.~\ref{fig:Tract}. Seven constraint names are also shown in the figure. These constraints are in charge of specifying statements on the source models~(e.g., proceedings should have at least one paper; persons should have unique names); and on the target models~(e.g., a book should have either an editor or an author, but not both).

%The constraints for the source are shown below. %in Listing~\ref{lst:Invs} below.
%
%
%\begin{lstlisting}[language=OCL, escapechar=§, numbers=none]
%context Person inv isAuthorOrEditor:
%  inProc->size() + proc->size() > 0
%context InProc inv booktitleOccursAsProcTitle:
%  Proc.allInstances->exists(prc | prc.title=booktitle)
%context Person inv uniqueName:
%  Person.allInstances->isUnique(name)
%context Proc inv hasAtLeastOnePaper:
%  InProc.allInstances->exists(pap | pap.booktitle=title)
%context Proc inv uniqueTitle:
%  Proc.allInstances->isUnique(title)
%context Proc inv withinProcUniqueTitle:
%  InProc.allInstances->select(pap |
%    pap.booktitle=title)->forAll(p1,p2 | p1<>p2 implies p1.title<>p2.title)
%context InProc inv titleDifferentFromPrcTitle:
%  Proc.allInstances->forAll(p| p.title<>title)
%\end{lstlisting}

In addition to constraints on the source and target models, tracts impose conditions on their relationship---as they are expected to be implemented by the transformation's execution. In this case, the \code{Tract} class serves to define the source-target constraints for the exemplar tract that we use~(although several tracts are normally defined for a transformation, each one focusing on specific aspects or use-cases of the transformation, for simplicity we will consider only one tract here). The following conditions are part of the source-target constraints of the tract:

\begin{lstlisting}[language=OCL, escapechar=§, numbers=none]
context t:Tract inv sameSizes:
  t.file->size() = t.docBook->size() and
  t.file->forAll( f | t.docBook->exists( db |
    f.entry->selectByType(Proc)->size() = db.book->size()))

context prc:Proc inv sameBooks:
  Book.allInstances->one( bk | prc.title = bk.title and
    prc.editor->forAll(pE | bk.editor->one( bE | pE.name = bE.name )))

context pap:InProc inv sameChaptersInBooks:
  Chapter.allInstances->one( chp |
    pap.title = chp.title and pap.booktitle = chp.book.title and
    pap.author->forAll(aP | chp.author->one(cA | aP.name=cA.name)) )
\end{lstlisting}
%context Tract inv sameTitles:
%  Tract->file->entry->collect(title) =
%    Tract->docBook->book->collect(title)


\subsection{Tract Test Suites}

In addition to the source, target and source-target tract constraints, \emph{test suites} play an essential role in Tracts. Test suite models are pre-defined input sets of different sorts aimed to exercise the transformation. Being able to select particular patterns of source models  offers a fine-grained mechanism for specifying the behaviour of the transformation, and allows the model transformation tester to concentrate on specific behaviours of the tract. It also enables test repeatability.
%Note that test suites may not only be positive test models, satisfying the source constraints, but also negative test models, used to know how the transformation behaves with them.

The generation of test suites for tracts has been traditionally achieved by random sampling, i.e., by selecting several object models at random, normally those selected by using different technologies for exploring all possible valid configurations (object models) of a metamodel, such as logic
programming and constraint solving~\cite{DBLP:conf/kbse/CabotCR07}, relational
logic and Alloy~\cite{DBLP:journals/sosym/AnastasakisBGR10}, term rewriting with
Maude~\cite{DBLP:journals/eceasst/RoldanD11} or graph grammars~\cite{Ehrig09}.

We also proposed the ASSL language (A Snapshot Sequence Language)~\cite{ASSL}, an imperative language developed to generate object diagrams for a given class diagram.  ASSL provides features for randomly choosing attribute values or association ends. Although quite powerful, imperative approaches such as this one are cumbersome to use and also present some limitations in practice~\cite{Offutt95}.
%, e.g., it is difficult to prove some of the properties that any test suite should exhibit, such as completeness~(are all possible sorts of input models covered?) and correctness~(are all generated models valid and correct?).
%In general, analysing the coverage of the test suite w.r.t. the given tract is far from being a trivial task.


\subsection{Determining the sample size}\label{sec:samplesize}

Several statistical works~\cite{SSW03,Sampling2012,Survey2009} show how the total sample size is calculated depending on several parameters, which include the size of the entire population ($N$), the required confidence level ($1-\alpha$) and the precision ($d$) of the resulting estimates. For example, one may wish to have the $95\%$ confidence interval be less than $0.06$ units wide (i.e., a required precision of $0.03$).

Confidence levels are given by the their corresponding critical values (also called $Z_\alpha$-values), which are defined in, e.g.,~\cite{Survey2009}. For example, the $Z_\alpha$-value for a confidence level of 95\% is $1.96$ and for a confidence level of 99\% is $2.58$.

 %the Table~\ref{tbl:confidencelevels} shows some common confidence levels and their corresponding $Z_\alpha$ values~\cite{Survey2009}.
%
%\begin{table}[t]
%  \centering
%  \caption{Confidence levels and \newline corresponding $Z_\alpha$ values.}\label{tbl:confidencelevels}
%  \begin{tabular}{|l|c|c|c|c|c|c|}
%    \hline
%    % after \\: \hline or \cline{col1-col2} \cline{col3-col4} ...
%    Confidence level & 80\% & 85\% & 90\% & 95\% & 98\% & 99\% \\ \hline
%    $Z_\alpha$ values & $1.28$ & $1.44$ & $1.64$ & $1.96$ & $2.33$ & $2.58$\\
%    \hline
%  \end{tabular}
%\end{table}

\begin{table}[b]
  \centering
  \caption{Examples of sample sizes according to different parameter values.}\label{tbl:samplesizes}
\begin{scriptsize}
  \begin{tabular}{|r|c|c|c|c|c|c|c|c|}
    \hline
 Confidence Level & 95\% & 99\% & 95\% & 99\% & 95\% & 99\% & 95\% & 99\%\\
 Failure rate (unkown=50\%) & 5\% & 5\% & 5\% & 5\% & 50\% & 50\% & 50\% & 50\%\\
 Precision (uncertainty) & 5\% & 5\% & 3\% & 3\% & 5\% & 5\% & 3\% & 3\%\\
 \hline
 \textbf{Resulting sample size} & \textbf{73} & \textbf{126} & \textbf{203} & \textbf{351} & \textbf{384} & \textbf{666} & \textbf{1,067} & \textbf{1,849}\\
    \hline
  \end{tabular}
\end{scriptsize}
\end{table}


Regarding the size $N$ of the entire population, in our case the population consists of all possible valid models, which is usually a very large and unknown number and then normally considered infinite for statistical purposes~\cite{SSW03}.

Finally, in this context we are concerned with the estimation of \emph{proportions}, since we can formulate the problem as the estimation of the ratio of models that fail the test, e.g., that are incorrectly transformed by the model transformation. The estimator of that ratio is  $p = X/n$, where $n$ is the sample size and $X$ is the number of successes, i.e., ``positive'' observations (e.g. the number of models for which the transformation fails).
%When the observations are independent, this estimator has a Normal distribution. % (and it is also the sample mean of data from a Bernoulli distribution).
If the ratio of positive observations is unknown, the recommended practice is to assume it to be $p = 0.5$.
%This implies the maximum variance of this distribution ($0.25/n$) and hence the more conservative case.


With all this, the formula that we have to use to determine the sample size $n$ is $n=(Z_\alpha^2 \times p \times (1-p))/d^2$. The result of the formula is always rounded to the next integer value. Table~\ref{tbl:samplesizes} shows some examples of sample sizes according to some of these parameters.

%, where:
%%%
%%\begin{equation}\label{eqn:samplesize}
%%  n=\frac{Z_\alpha^2 \times p \times q}{d^2}
%%\end{equation}
%%
%%\noindent
%
%\begin{itemize}
%  \item $Z_\alpha$ is the critical value for the corresponding confidence level.
%  \item $d$ is the required precision.
%  \item $p$ is the proportion of positive observations.
%  \item $q$ is the proportion of negative observations ($q=1-p$).
%\end{itemize}



%For example, suppose that we want to determine the sample size $n_1$, with a confidence level of $95\%$ and a precision of $3\%$, for a model transformation that has been tested before, and we expect to fail only with at most $5\%$ of the input models. Then, formula~(\ref{eqn:samplesize}) yields the following result:
%%
%\begin{equation}\label{eqn:n1}
%  n_1=\frac{1.96^2 \times 0.05 \times 0.95}{0.03^2}=203
%\end{equation}
%
%If we want to increase the confidence level to $99\%$, the sample size goes up to $352$ models:
%\begin{equation}\label{eqn:n2}
%  n_2=\frac{2.58^2 \times 0.05 \times 0.95}{0.03^2}=352
%\end{equation}
%
%And if we want to maintain the confidence level in $95\%$ and the precision in $3\%$, but we do not know the expected behaviour of the model transformation and therefore we can expect it to fail with a probability of $p=0.5$, the number of models $n_3$ to include in the sample is:
%\begin{equation}\label{eqn:n3}
%  n_3=\frac{1.96^2 \times 0.5 \times 0.5}{0.03^2}=1068
%\end{equation}
%
%Note that since we are using bounded model checking, we do not have an infinite population, i.e., the model search space given by the bounds defines a finite number of system states. For very small models, e.g., those that have less system states than representative elements required by the stratified sampling, and for which the size of the population $N$ (i.e., the total number of system states) is known and small, another formula can be used:
%%
%\begin{equation}\label{eqn:samplesize2}
%  n=\frac{N \times Z_\alpha^2 \times p \times q}{d^2\times(N-1)+Z_\alpha^2 \times p \times q}
%\end{equation}
%
%However, this formula is expected to be rarely used in practice since a model with a small and bounded number of system states does not normally require the use of these sampling techniques. For larger models, formula~(\ref{eqn:samplesize}) should be used to determine how many test cases are required in the test suite.
%
%

\subsection{Classifying terms}\label{sec:CTs}

Classifying terms~\cite{Gogolla2015} constitute a technique for developing test cases for UML and OCL models, based on an approach that automatically constructs object models for class models enriched by OCL constraints. By guiding the construction process through so-called classifying terms, the built test cases in form of object models which are classified into equivalence classes.

Classifying terms are arbitrary OCL expressions on a class model that calculate a characteristic value for each object model.
The expressions can either be boolean, allowing to define up to two equivalence classes; or numerical, where each resulting number defines one equivalence class. Each equivalence class is then defined by the set of object models with identical characteristic values and by one canonical representative object model. %By inspecting these object models, a developer can explore properties of the class model and its constraints.

%In other words, classifying terms provide an instrument to explore model properties.
%They are employed in the model validation and verification process when one
%has a UML and OCL model given, and one has in addition a so-called
%configuration that specifies a finite search space connecting the
%models elements, basically classes, associations, attributes, and
%datatypes, with finite sets of possible populations. As an important
%validation task, the developer might want to see all object models
%that satisfy the UML and OCL class model and that fit to the
%configuration. Classifying terms give the developer an explicit option
%to formulate their understanding of two object models being
%different. Thus when the developer scrolls through all object models
%matching the UML class model, the OCL invariants and the
%configuration, they will only encounter models with different
%characteristics with respect to the classifying term.

%The technical realization works as follows. The developer specifies a
%closed OCL query term, i.e., a term without free variables, that can
%be evaluated in an object model and that returns a characteristic
%value. This term is called \emph{classifying term}. Each newly constructed
%object model has to show a different characteristic value for the
%classifying term. The classifying term determines an equivalence
%relationship on all object models. Two object models with the same
%characteristic value belong to the same equivalence class.
%
%In our context, the equivalence classes determine the kinds (or subgroups) of models in which we are interested, i.e., the different strata.

%It is important to remark that the equivalence classes defined by the classifying terms define a \emph{partition} of the whole object model space. That is, every object model belongs to one and only one equivalence class.
%Or, in other words, the pairwise intersection of the equivalence classes is empty and their union covers the whole model space.

%The definition of the classifying terms corresponds to the mapping that the developer has in mind about the transformation, and how it should map source models of each source equivalence class into target models of the corresponding target class. In this sense, the source and target classifying terms should be `in correspondence'. Our approach can also help checking such expected mappings.
%%%for each completion $t_{ik}$ of a source object model $s_i$, there must exist a target object model $t_j$ that shows the same  behavior w.r.t. to the classifying term. In other words,
%%if we take a source model $s_i$ generated from a CT, complete it to the corresponding target model set $T_i=\{t_{i1},\dots,t_{in}\}$, for every of these target models $t_{ik}$ there should be a target model $t_j$ from the set of target models generated by the target CTs that belongs to the same equivalence class. Otherwise, either the MT is faulty, or the CTs are badly chosen.
%%%fulfil the Transformation model
%%%
%%%  sm |---> comp(sm) = completed sm reduced to the target part
%%%
%%%             tm_1 \
%%%             ...   generated by the target CTs
%%%             tm_k /
%%%
%%%           classifTerm[comp(sm)] = classifTerm[tm_i] for some i in 1..k
%%%If there is no such target model, the `in correspondence' supposition, and with this the CTs, are badly chosen.
%%%Analogously, one can also try to consider the direction from target to source with such a completion technique.
%%The same process can be followed considering the direction from target to source, checking the opposite direction of the transformation model.

%\subsection{Building Tract Test Suites with Classifying Terms}\label{sec:CTsForTracts}

%As mentioned above, classifying terms  permit guiding the construction process of the test suites using \emph{equivalence classes} that determine the sorts of input models of the tract.
The process to build the test suite is straightforward. We begin by identifying the \emph{sorts} of models that we would like to be part of the test suite. Each sort is specified by a classifying term, that represents the equivalence class with all models that are \emph{equivalent} according to that class, i.e., which belong to the same sort. Once the classifying terms are defined for a tract, the USE tool generates one representative model for each equivalence class. These \emph{canonical} models constitute the test suite of the tract.

For example, suppose that we want to concentrate on different characteristics of the input models of the \code{BibTex2DocBook} transformation. First, proceedings have two dates: the year in which the conference event was held~(\code{yearE}) and the year in which the proceedings were published~(\code{yearP}). We want to have input models in which these two dates coincide in at least one proceeding, and other input models for which the conference event and publication years are always different. Second, we want to have some sample input model in which one person is editor of at least one proceeding and also author of at least one inproceeding, and also models in which the persons are either author or editors, but not both.
Finally, we want to have some source models with at least one inproceeding whose title coincides with its booktitle, and other input models where the titles and booktitles of inproceedings are always different.

As mentioned above, producing test suite models to cover all these circumstances by an imperative approach or by ASSL is normally tedious and error prone. However, the use of classifying terms greatly simplifies this task. It is enough to give three Boolean terms to the model validator, each one defining the classifying term that specifies the characteristic we want to identify in the model. In this case, these Boolean terms are the ones shown below.

\begin{lstlisting}[language=OCL, escapechar=§, numbers=none]
[ yearE_EQ_yearP ]
    Proc.allInstances->exists(yearE=yearP)

[ twoRolePerson ]
    Person.allInstances->exists (proc->size>0 and inProc->size>0)

[ title_EQ_booktitle ]
    BibTeXEntry.allInstances->exists(be |
        InProc.allInstances->exists(ip | be.title=ip.booktitle))
\end{lstlisting}

Given that an object model either belongs or not to any of the equivalence classes, the three classifying terms define eight equivalence classes ($8 = 2^3$) and the the model validator selects one representative for each one of them by scrolling through all valid object models. For this, as described in~\cite{Gogolla2015}, each classifying term is assigned an integer value, and the values of the classifying terms are stored for each solution. Using the classifying terms and these values, constraints are created and given to a Kodkod solver along with the class model during the validation process. %Informally, the constraint schema reads as follows: there exists no previous object model for which the
%evaluation of all classifying terms in the object model currently under construction equals the stored values of the previous object
%models. With this, the solver prunes all object models that belong to equivalence classes for which there is already a representative element.
%
%The USE model validator is able to handle developments tasks realizing
%finite bounded model checking. The validator constructs object
%diagrams for a UML class diagram enriched by OCL invariants and is
%based on relational logic. The validator has to be instructed by a
%so-called configuration that determines how the classes, associations,
%datatypes and attributes are populated. In particular, for every class
%a mandatory upper bound for the number of objects must be stated.
Both the USE tool and the model validator plugin are available for download from \url{http://sourceforge.net/projects/useocl/}.

%, which are shown in Fig.~\ref{fig:bite2dobo_sol_1-8} in the
%order the model validator finds them. For each solution the value of the three properties~(\code{yearE\_EQ\_yearP},
%\code{noManusManumLavat}, \code{noSelfEditedPaper}) is indicated in the figure with integer
%values $(0,1)$, indicating whether that solution fulfills the condition~($1$) or not~($0$).

%%\textbf{Equivalence partitioning:}
%In summary, we have been able to define a set of $8$ equivalence classes that characterize the sorts of input models we are interested in, and have the model validator find representative~(i.e., canonical) models for each class. In this way we make sure the models that constitute the tract test suite cover all cases of interest.

%\subsection{Further Analysis of Model Transformations}\label{sec:furtherAnalyses}
%
%\begin{figure}[t]
%\centering
%\includegraphics[width=.5\linewidth]{images/partition}
%\caption{Classifying terms for defining partitions of source and target spaces.}
%\label{fig:partition}
%\end{figure}
%
%Due to the way in which classifying terms can be specified~(by means of Boolean terms) for building the tract test suites models, they define a set of equivalence classes that constitute a~(complete and disjoint) partition of the input model space of the transformation. This is useful to select sample input models of different sorts~(one per equivalence class), making sure that $(a)$~we do not miss any representative model from any sort of model of interest~(completeness), and $(b)$~no two sample models are of the same kind~(disjointness), as pictured in~Fig.~\ref{fig:partition}.
%
%But we can also apply the idea of partitioning a model space with the target domain, characterizing the sorts of target models which are of certain interest to the modeler~(or to the model transformation tester). The equivalence classes defined by the target classifying terms are very useful for checking several properties of the transformation. For example, we could check that:
%
%\begin{itemize}
%  \item All sorts of target models of interest are produced by the transformation---i.e., \emph{full coverage} of certain parts the target model space.
%  \item No target models of certain forms~(sorts) are produced because they would be invalid target models---i.e., the transformation produces \emph{no junk}.
%  \item No target models of certain sorts are mapped to the same sort of target model when they shouldn't---i.e., the transformation introduces \emph{no confusion} when it shouldn't~(two models are not mapped to equal target sorts unless they belong to the same source sort).
%\end{itemize}
%
%
%To illustrate this, let us go back to the \code{BibTeX2DocBook} transformation, where we can identify some sorts of models of interest in the target model space.
%For instance, we can be interested in a property that was also of relevance in the source target space, such as \emph{self edited papers}~(i.e., whether the editor of a book is also the author of one of the chapters). We can also be interested in \emph{normal} books, i.e., those which are not composition of papers selected by an editor, but instead all chapters are written by the same person, the book author. Finally, books in which no author writes more than one paper could be of interest too.
%In order to specify these properties and define the appropriate equivalence classes we just need to write the corresponding classifying terms:
%
%\begin{lstlisting}[escapechar=§, numbers=none]
%[ noSelfEditedPaper ]
%  not Book.allInstances->exists(b |
%    b.editor->intersection(b.chapter.author)->notEmpty() )
%
%[ onlyNormalBooks ]
%  Book.allInstances->forAll(b |
%    b.editor->isEmpty() and b.chapter->forAll(c | c.author=b.author))
%
%[ noRepeatedAuthors ]
%  Book.allInstances()->forAll(b |
%    b.chapter->forAll( c1, c2 |
%      c1 <> c2 implies c1.author->intersection(c2.author)->isEmpty()))
%\end{lstlisting}
%
%
%%[ moreThanOneChapter ]
%%  Book.allInstances()->forAll(b |
%%    b.chapter->size() > 1 )
%
%%  Book.allInstances->forAll(b |
%%    b.author->forAll(a |
%%    a.chapter->select(book=b)->size()=1 ) )
%
%
%These three boolean classifying terms produce only $6$ equivalence classes in the target model space, instead of the $8$~($8=2^3$) that could be expected. This is because self-edited papers cannot be at the same time normal books, i.e., negation of \code{noSelfEditedPaper} and \code{onlyNormalBooks} exclude each other.
%
%It is now a matter of determining the expected behaviour of the transformation with the input models from the source equivalence classes. In this respect, there are properties that should be preserved by the transformation~(e.g., \code{noSelfEditedPaper}) and others that cannot happen~(e.g., given that proceedings must have at least one editor, no normal book can be generated by the transformation).
%
%In order to check that, it is a matter of analysing the behaviour of the model transformation with the representative models of each source equivalence class. Thus, with the set of equivalence classes in the source and target model spaces, we can execute the model transformation on the test suite and check whether the output models belong to the appropriate equivalence classes in the target model space.
%
%In this case, the mapping done by transformation for the $8$ representative source models of the equivalence classes~(which are shown in Fig.~\ref{fig:bite2dobo_sol_1-8}) is as described by Fig.~\ref{tab:transf}.
%
%\begin{figure}[t]
%\centering
%\small
%\begin{tabular}{ccc}
%\textrm{Source} && \textrm{Target} \\ \cline{1-1} \cline{3-3}\noalign{\smallskip}
%$[0,0,0]$ & $\longrightarrow$ & $[0,0,1]$ \\
%$[0,0,1]$ & $\longrightarrow$ & $[0,0,1]$ \\
%$[0,1,0]$ & $\longrightarrow$ & $[0,0,1]$ \\
%$[0,1,1]$ & $\longrightarrow$ & $[0,0,0]$ \\
%$[1,0,0]$ & $\longrightarrow$ & $[1,0,1]$ \\
%$[1,0,1]$ & $\longrightarrow$ & $[1,0,1]$ \\
%$[1,1,0]$ & $\longrightarrow$ & $[1,0,0]$ \\
%$[1,1,1]$ & $\longrightarrow$ & $[1,0,1]$ \\
%\end{tabular}
%\caption{Mapping equivalence classes.}
%\label{tab:transf}
%\end{figure}
%
%In the table, each equivalence class is represented by a tuple $[x_1,x_2,x_3]$ where \mbox{$x_i \in \{0,1\}$} indicates if the model satisfies condition $i$ of the corresponding classifying term. Thus, in the source model space the tuple $[1,1,1]$ means that model satisfies \code{noSelfEditedPaper}, \code{noManusManumLavat} and \code{yearE\_EQ\_yearP}, while in the target model space the tuple $[1,1,1]$ corresponds to a model that satisfies conditions \code{noSelfEditedPaper}, \code{onlyNormalBooks} and \code{noRepeatedAuthors}~(in this order).
%
%Thus we can see how in effect no normal books have been produced when the transformation is executed on the source models. We can also see that with these input models, all the rest of the equivalence classes that we have defined for the target space have been reached.
%
%Possible misbehaviours of a model transformation detected using this approach may be due to several causes. In the first case, the equivalence classes of the transformed models in the target model space do not coincide with the expected ones. This would mean a problem in the implementation of the transformation. But it could also be the case of a wrong definition of the source or target classifying terms, which would uncover a potential mistake in the way the designer expects the transformation to work. %As mentioned in the introduction, one of the current problems of model transformations is that their specifications can be as complex as their implementations. Hence the need to count on abstraction mechanisms that allow the designer to manage that complexity at the right level of abstraction.
%In this respect, the model validator can also be very useful to find counterexamples for situations that in principle should not happen, but that are permitted by our specification because either the classifying terms or even the tracts themselves are not properly defined, as discussed in~\cite{DBLP:conf/models/HilkenBGV15}.


