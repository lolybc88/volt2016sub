

\subsection{Determining the sample size for each stratum}

Once we know the complete sample size, we need to decide the number of elements of each stratum. There are two main approaches here, depending on the knowledge we have about the strata or on the estimations we can make about them.

In the simplest case (called proportional allocation), the sample size of each stratum respects the proportion of elements that each stratum represents from the entire population. Hence, the more elements belong to a group, the larger the sample size for the stratum representing that group.

For this we need to know (or estimate) the proportion of each stratum. One usual method is to use some a priori knowledge about the population to estimate these proportions. There are also several techniques that can be applied in case we do not have any prior knowledge or we do not want to guess, as described in~\cite{Sampling2012}. One common technique consists of generating some random samples first and then use them to estimate the proportions.

In our example, we had defined $8$ different equivalence classes, corresponding to the $3$ classifying terms specified for the \code{Persons} metamodel. Table~\ref{tbl:proportions} lists these classes and the proportions that we have estimated for them.

\begin{table}[t]
  \centering
  \caption{Equivalence classes and their estimated proportions.}\label{tbl:proportions}
  \begin{tabular}{|l|c|c|c|c|}
    \hline
        & atLeast30Elems & noSelfEditedPaper & yearE\_EQ\_yearP & \\ \hline
      1 & true & true & true & 33\% \\ \hline
      2 & true & true & - & 2\% \\ \hline
      3 & true & - & true & 14\% \\ \hline
      4 & true & - & - & 1\% \\ \hline
      5 & - & true & true & 27\% \\ \hline
      6 & - & true & - & 1\% \\ \hline
      7 & - & - & true & 21\% \\ \hline
      8 & - & - & - & 1\% \\ \hline
    % after \\: \hline or \cline{col1-col2} \cline{col3-col4} ...
  \end{tabular}
\end{table}

With these proportions, if the total sample size is $203$ the corresponding sample sizes of each stratum should be, respectively: $67$, $5$, $29$, $3$, $55$, $3$, $43$
and $3$ (they add $208$, but remember that these numbers are always rounded up).

In summary, we are able to know the number of object models that need to be generated for each equivalence class based only on the required confidence level and precision, and on the estimated proportions of each class.
%The first two parameters are normally determined by the margin of error we are suppose to tolerate. The last parameters, the proportions, may require further studies unless we use our intuition.

There are other allocation techniques for estimating the sample size for each stratum, beyond proportional allocation. For example, disproportionate allocation assumes that the size of each sample should be proportionate to the standard deviation of the distribution of the variable, and not to the size of the population. With this, larger samples are taken for the strata with greater variability, in order to generate the least possible sampling variance. Other allocation techniques also take into consideration the cost of generating each sample and therefore they introduce other formulae for estimating the sample size for each stratum. In this paper we have focused on the use of a proportional allocation technique, which is the most common one. References  \cite{SSW03,Survey2009,Sampling2012} describe other allocation techniques and their corresponding estimators.


\subsection{Positive observations in each stratum}

The previous calculations in formulae~(\ref{eqn:n1}) to~(\ref{eqn:n3}) assume that the probability of positive observations is the same for all strata. However, this may not be the case because we may know that the model operation (for instance the model transformation under test) is more likely to fail with models from certain strata. Thus, imagine that we have $H$ strata and that the proportion of elements of stratum $i$ with respect to the entire population is $r_i$ $(0\leq r_i\leq 1, \sum_{i=1}^Hr_i=1)$.

Suppose that the proportion of positive observations in stratum $i$ is $p_i$. In other words, that the probability of the model operation to fail with samples from stratum $i$ is $p_i$ $(0\leq p_i\leq 1)$. With this, the proportion of positive observations $p$ in formula~(\ref{eqn:samplesize}) should be computed as $p=\sum_{i=1}^Hr_ip_i$. Again, $q=1-p$.

%\subsection{Other allocation techniques}
%
%Other allocation technique for estimating the sample size for each stratum assumes that each one is proportionate to the standard deviation of the distribution of the variable, and not to the size of the population. This is called disproportionate allocation. With it, larger samples are taken for the strata with greater variability, in order to generate the least possible sampling variance. Other allocation techniques take into consideration the cost of generating each sample and therefore introduce other formulae for estimating the sample size for each stratum. The interested reader can consult references \cite{SSW03,Survey2009,Sampling2012} for the description of other allocation techniques and their corresponding estimators.


%\begin{equation}\label{samplesize4}
%  p=\sum_{i=1}^Hr_ip_i
%\end{equation}
%\noindent and then, as before, $q=1-p$.

\subsection{Putting all pieces together}

To wrap up all concepts and mechanisms described in the paper, let us present here how a system modeler is supposed to use them for generating a test suite of object models.

Starting with the system metamodel, including its associated constraints, the modeler has to identify first the different \emph{kinds} of object models that are of interest to the model operation under test. These kinds are represented by equivalence classes, which in turn are specified by means of classifying terms.

The next step is to determine the number of test cases (i.e., objects models) that need to be generated in total. This mainly depends on the precision and confidence level required, and can be obtained using formula~(\ref{eqn:samplesize}).

Then, we need to estimate for each equivalence class (or stratum), the proportion that its elements represent with respect to the total population of object models (see, e.g., Table~\ref{tbl:proportions}). With these figures we are in a position to know the number of test cases for each classifying term.

Finally, it is a matter of asking the USE model validator to generate the number of object models for each equivalence class, following the process described at the end of Section~\ref{sec:ExtendingCTs}.

